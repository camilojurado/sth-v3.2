﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Entrada : MonoBehaviour
{
    public string NombreDeLaEcena;
    public string CheckPointTag;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D Colicion)
    {
        if (Colicion.CompareTag("Player"))
        {
            if (CheckPointTag != "")
            {
                PlayerPrefs.SetString("PosicionInicial", CheckPointTag);
            }
            SceneManager.LoadScene(NombreDeLaEcena);
        }
    }  
}
