﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Puntos : MonoBehaviour
{
    public int ValPunTos = 0;

    
    public Text PunTos;
    
    // Start is called before the first frame update
    void Start()
    {
        PunTos = GetComponent<Text>();
        ValPunTos = PlayerPrefs.GetInt("Anillos", 0);
    }

    // Update is called once per frame
    void Update()
    {
        PunTos.text = "Rings : " + ValPunTos;
    }
}
