﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorSilver : MonoBehaviour
{
    private Animator animator;
    public float speed;
    public float Jumpforce;
    public LayerMask groundlayer;
    private Rigidbody2D rigidBody2D;
    private Vector3 Scale;
    
    public bool Levitando;
    public bool Ball;
    public bool Dash;
    public bool Golpeando;
    public bool Explocion;
    
    public GameObject Bloque;
    public float MuelleForce;
    public float MuelleForceRojo;
    public float DashSpeed;
    public AudioClip Saltando;

    public GameObject ChackPoint;

    
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidBody2D = GetComponent<Rigidbody2D>();
        Scale = transform.localScale;
        if (PlayerPrefs.GetString("PosicionInicial", "") != "")
        {
            transform.position = GameObject.FindGameObjectWithTag(PlayerPrefs.GetString("PosicionInicial", "")).transform.position;
        }
        PlayerPrefs.SetString("PosicionInicial", "");
    }

    // Update is called once per frame
    void Update()
    {
        
        if (Levitando)
        {
            animator.SetBool("Tele-Elevacion", true);
            MovimientoLevitacion();
        }
        else
        {
            //if ()
            animator.SetBool("Tele-Elevacion", false);
            MovimientoBasico();
        }

        if (Input.GetKeyUp(KeyCode.T)&& Bloque != null)
        {
            Levitando = false;
            Bloque.AddComponent<Rigidbody2D>();
            Bloque.transform.SetParent(null);
        }



       

    }


    public bool Coliciones()
    {

        if (Physics2D.Raycast(transform.position,Vector2.down,0.5f,groundlayer.value))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private void OnTriggerStay2D(Collider2D colision)
    {
        if (colision.CompareTag("Bloque"))
        {
            Bloque = colision.gameObject;
            Debug.Log("Colicion");
            if (Input.GetKey(KeyCode.T))
            {
                Levitando = true;
                Destroy(Bloque.GetComponent<Rigidbody2D>());
                Bloque.transform.SetParent(transform);
            }
            /*else
            {
                Levitando = false;
                Bloque.transform.SetParent(null);
                
            }
            */
        }
    }
    private void OnTriggerEnter2D (Collider2D colision)
    {

        if (colision.CompareTag("Muelle"))
        {

            rigidBody2D.AddForce(Vector2.up * MuelleForce, ForceMode2D.Impulse);
        }
        if (colision.CompareTag("Muelle rojo"))
        {

            //Vector3 Direccion = colision.transform.localPosition;
            Vector3 Direccion = colision.transform.up;


            rigidBody2D.AddForce(Direccion * MuelleForce, ForceMode2D.Impulse);
        }
        if (colision.CompareTag("CaidaLava"))
        {
            transform.position = ChackPoint.transform.position;
        }
    }
    void MovimientoBasico()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        if (Horizontal != 0)
        {
            if (Horizontal > 0)
            {
                transform.localScale = new Vector3(Scale.x, Scale.y, Scale.z);
            }
            else
            {
                transform.localScale = new Vector3(-Scale.x, Scale.y, Scale.z);
            }

            rigidBody2D.velocity = new Vector2(Horizontal * speed, rigidBody2D.velocity.y);
        }

        
        if (Coliciones())
        {
            
            if (Horizontal != 0)
            {
                animator.SetBool("Movimiento", true);
            }
            else
            {
                animator.SetBool("Movimiento", false);
            }

            if (Input.GetKey(KeyCode.UpArrow) && Coliciones())
            {

                
                rigidBody2D.AddForce(Vector2.up * Jumpforce, ForceMode2D.Impulse);
                SoundManager.PlaySfx(Saltando);
                animator.SetBool("Salto", true);
            }
            else
            {

                animator.SetBool("Salto", false);
            }

        }
        if (Input.GetKey(KeyCode.R))
        {
          
            animator.SetBool("Dash", true);
            rigidBody2D.velocity = new Vector2(Horizontal * DashSpeed, rigidBody2D.velocity.y);
            Debug.Log("Dash");
            
        }
        else
        {
            animator.SetBool("Dash", false);
            
        }
        
            if (Input.GetKeyDown(KeyCode.B))
            {
                animator.SetBool("Ball", true);
                Debug.Log("Ball");
            }
            else
        {
            animator.SetBool("Ball", false);
        }
            
            



        if (Input.GetKeyDown(KeyCode.H))
        {
            
            animator.SetTrigger("Golpeando");
            Debug.Log("Golpe");
            Golpeando = true;
        }
        else
        {
            Golpeando = false;
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
            animator.SetTrigger("Explocion");
            Debug.Log("Pis Boom");
        }

        


    }
    void MovimientoLevitacion()
    {
        float Horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        if (Horizontal != 0)
        {
            if (Horizontal > 0)
            {
                transform.localScale = new Vector3(Scale.x, Scale.y, Scale.z);
            }
            else
            {
                transform.localScale = new Vector3(-Scale.x, Scale.y, Scale.z);
            }

            rigidBody2D.velocity = new Vector2(Horizontal * speed, rigidBody2D.velocity.y);
        }
        if (Vertical != 0)
        {
            rigidBody2D.velocity = new Vector2(rigidBody2D.velocity.x, Vertical * speed);
        }

    }


}

// Vertical
// KeyCode.Space
// KeyCode.
// GetKeyUp
// GetKeyDown
// GetKey
// AddForce
