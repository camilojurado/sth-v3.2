﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Botones : MonoBehaviour
{
    public GameObject PantallaDeOpciones;
    public enum TipoDeBoton
    {
        Start,
        Optiones,
        Salir,
    }
    public TipoDeBoton boton;

    public string NameScene;
    public void Clik()
    {
        if (boton == TipoDeBoton.Start)
        {
            PlayerPrefs.SetInt("Anillos", 0);
            SceneManager.LoadScene(NameScene);
        }
        else if (boton == TipoDeBoton.Optiones)
        {
            PantallaDeOpciones.SetActive(true);
            Debug.Log("Boton de opciones apretado");
        }
        else if (boton == TipoDeBoton.Salir)
        {
            Application.Quit();
            Debug.Log("Boton de salir apretado");
        }

        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
