﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Anillos : MonoBehaviour
{
    public AudioClip Recoleccion;
    public Puntos ManagerPuntos;
    public int Puntos;
    public bool Box;

    
    // Start is called before the first frame update
    void Start()
    {
        ManagerPuntos = FindObjectOfType<Puntos>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D (Collider2D Colicion)
    {
        if (Colicion.CompareTag("Player"))
        {
            SoundManager.PlaySfx(Recoleccion);
            
            if (Colicion.GetComponent <ControladorSilver>().Golpeando && Box)
            {
                ManagerPuntos.ValPunTos += Puntos;
                Destroy(gameObject);

            }
            else
            {
                ManagerPuntos.ValPunTos += Puntos;
                Destroy(gameObject);
            }

            PlayerPrefs.SetInt("Anillos", ManagerPuntos.ValPunTos);
            
            
        }



    }


}
